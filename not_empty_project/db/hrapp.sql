-- MySQL dump 10.13  Distrib 8.0.22, for macos10.15 (x86_64)
--
-- Host: 127.0.0.1    Database: hrapp
-- ------------------------------------------------------
-- Server version	8.0.22

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `assessments`
--

DROP TABLE IF EXISTS `assessments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `assessments` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `user` bigint unsigned NOT NULL,
  `name` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user` (`user`),
  KEY `id` (`id`),
  KEY `id_2` (`id`),
  CONSTRAINT `assessments_ibfk_1` FOREIGN KEY (`user`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `assessments`
--

LOCK TABLES `assessments` WRITE;
/*!40000 ALTER TABLE `assessments` DISABLE KEYS */;
INSERT INTO `assessments` VALUES (1,1,'Мероприятие №1');
/*!40000 ALTER TABLE `assessments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `assessments_connections`
--

DROP TABLE IF EXISTS `assessments_connections`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `assessments_connections` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `assessment` bigint unsigned NOT NULL,
  `user_from` bigint unsigned NOT NULL,
  `user_to` bigint unsigned NOT NULL,
  `assessments_form` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_from` (`user_from`),
  KEY `assessment` (`assessment`),
  KEY `assessments_connections_ibfk_2` (`user_to`),
  CONSTRAINT `assessments_connections_ibfk_1` FOREIGN KEY (`user_from`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  CONSTRAINT `assessments_connections_ibfk_2` FOREIGN KEY (`user_to`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  CONSTRAINT `assessments_connections_ibfk_3` FOREIGN KEY (`assessment`) REFERENCES `assessments` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `assessments_connections`
--

LOCK TABLES `assessments_connections` WRITE;
/*!40000 ALTER TABLE `assessments_connections` DISABLE KEYS */;
/*!40000 ALTER TABLE `assessments_connections` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `competencies`
--

DROP TABLE IF EXISTS `competencies`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `competencies` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `user` bigint unsigned NOT NULL,
  `name` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user` (`user`),
  KEY `id` (`id`),
  KEY `id_2` (`id`),
  CONSTRAINT `competencies_ibfk_1` FOREIGN KEY (`user`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `competencies`
--

LOCK TABLES `competencies` WRITE;
/*!40000 ALTER TABLE `competencies` DISABLE KEYS */;
INSERT INTO `competencies` VALUES (9,1,'Знание основ языка JS'),(10,1,'Умение работать с СУБД'),(12,1,'Умение работать с API'),(13,1,'Понимание работы с библиотекой React JS'),(14,1,'Понимание работы с фреймворком expressJS');
/*!40000 ALTER TABLE `competencies` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `criteria`
--

DROP TABLE IF EXISTS `criteria`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `criteria` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `competence` bigint unsigned NOT NULL,
  `text` text NOT NULL,
  `typecheckbox` int unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `competence` (`competence`),
  KEY `id` (`id`),
  CONSTRAINT `criteria_ibfk_1` FOREIGN KEY (`competence`) REFERENCES `competencies` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `criteria`
--

LOCK TABLES `criteria` WRITE;
/*!40000 ALTER TABLE `criteria` DISABLE KEYS */;
INSERT INTO `criteria` VALUES (2,9,'Из перечисленных ниже пунктов, выберите те, которым по вашему мнению соответствует оцениваемый специалист.',1),(3,13,'Из перечисленных ниже пунктов, выберите те, которым по вашему мнению соответствует оцениваемый специалист.',1),(4,14,'Из перечисленных ниже пунктов, выберите те, которым по вашему мнению соответствует оцениваемый специалист.',1),(5,12,'Из перечисленных ниже пунктов, выберите том, которому по вашему мнению соответствует оцениваемый специалист.',0),(6,10,'Из перечисленных ниже пунктов, выберите том, которому по вашему мнению соответствует оцениваемый специалист.',0);
/*!40000 ALTER TABLE `criteria` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `moderator_access`
--

DROP TABLE IF EXISTS `moderator_access`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `moderator_access` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `useraccess` bigint unsigned NOT NULL,
  `redactforms` int unsigned NOT NULL,
  `redactstaff` int unsigned NOT NULL,
  `makeevents` int unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `useraccess` (`useraccess`),
  CONSTRAINT `moderator_access_ibfk_1` FOREIGN KEY (`useraccess`) REFERENCES `users_access` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `moderator_access`
--

LOCK TABLES `moderator_access` WRITE;
/*!40000 ALTER TABLE `moderator_access` DISABLE KEYS */;
INSERT INTO `moderator_access` VALUES (2,1,1,1,1);
/*!40000 ALTER TABLE `moderator_access` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `results`
--

DROP TABLE IF EXISTS `results`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `results` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `assessment` bigint unsigned NOT NULL,
  `content` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `assessment` (`assessment`),
  CONSTRAINT `results_ibfk_1` FOREIGN KEY (`assessment`) REFERENCES `assessments` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `results`
--

LOCK TABLES `results` WRITE;
/*!40000 ALTER TABLE `results` DISABLE KEYS */;
INSERT INTO `results` VALUES (1,1,'ggIO0rpOA'),(2,1,'SMzHfvsas'),(3,1,'zqpxUcDLV'),(4,1,'X2Zvl50aN'),(5,1,'TUZggAVLx'),(6,1,'asMC-sDgj'),(7,1,'q6qX0wRM6'),(8,1,'UmTQaaSY5'),(9,1,'UsCM0R_5n');
/*!40000 ALTER TABLE `results` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `signs`
--

DROP TABLE IF EXISTS `signs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `signs` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `criterion` bigint unsigned NOT NULL,
  `text` text NOT NULL,
  `weight` int unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `criterion` (`criterion`),
  CONSTRAINT `signs_ibfk_1` FOREIGN KEY (`criterion`) REFERENCES `criteria` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `signs`
--

LOCK TABLES `signs` WRITE;
/*!40000 ALTER TABLE `signs` DISABLE KEYS */;
INSERT INTO `signs` VALUES (4,2,'Понимание приведения типов.',20),(5,2,'Понимание работы с функциями.',20),(6,2,'Умение работы с методами перебора массивов.',20),(7,2,'Умение работать с асинхронными операциями.',20),(8,2,'Умение работать с классами.',20),(9,3,'Структурирование данных приложения с помощью состояния.',20),(10,3,'Структурирование логики приложения с помощью компонентов.',20),(11,3,'Грамотное использование компонентов высшего порядка.',20),(12,3,'Грамотное использование state-менеджеров.',20),(13,3,'Умение использовать хуки и функциональные компоненты.',20),(14,4,'Грамотное написание обработчиков.',25),(15,4,'Грамотное использование middleware.',25),(16,4,'Грамотная обработка ошибок.',25),(17,4,'Грамотная интеграция с базами данных.',25),(21,6,'Можете ли вы утверждать, что оцениваемый специалист понимает разницу между реляционными и не реляционными базами данных, свободной владеет языком запросов, быстро строит сложные запросы?',0),(22,6,'Можете ли вы утверждать, что оцениваемый специалист умело использует на практике запросы, но обладает недостаточно высокой технической базой и делает ошибки?',0),(23,6,'Можете ли вы утверждать, что оцениваемый специалист не разбирается в работе с СУБД?',0),(24,5,'Можете ли вы утверждать, что оцениваемый специалист свободно использует api разных типов и разбирается в инструкциях по их использованию?',100),(25,5,'Можете ли вы утверждать, что оцениваемый специалист знаком с работой с api, но не использует все её возможности?',60),(26,5,'Можете ли вы утверждать, что оцениваемый специалист не разбирается в работе с api?',0);
/*!40000 ALTER TABLE `signs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tokens`
--

DROP TABLE IF EXISTS `tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tokens` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `user` bigint unsigned NOT NULL,
  `token` text NOT NULL,
  `date` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user` (`user`),
  CONSTRAINT `tokens_ibfk_1` FOREIGN KEY (`user`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tokens`
--

LOCK TABLES `tokens` WRITE;
/*!40000 ALTER TABLE `tokens` DISABLE KEYS */;
INSERT INTO `tokens` VALUES (1,1,'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJsb2dpbiI6ImFkbWluXyIsInBhc3N3b3JkIjoiMTExMTExIiwiaWF0IjoxNjA4OTA3NDUzfQ.5UP9G-jUJSO-yi0OkcgCYzNptMv0srCQzJpr5zX0uX4','20201225164413'),(2,1,'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJsb2dpbiI6ImFkbWluXyIsInBhc3N3b3JkIjoiMTExMTExIiwiaWF0IjoxNjA4OTA3NTE0fQ.SJooVrBa6snRJ8AGFg3XE0tDE-S7gL_4G122KcwQXQA','20201225164514'),(3,2,'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJsb2dpbiI6InVzZXIwMDEiLCJwYXNzd29yZCI6IjExMTExMSIsImlhdCI6MTYwODkwNzY5OH0.aqPjawooFs0Q3pUEh3MQlxizVnIRRbsQo36ci4UnsYI','20201225164818'),(4,2,'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJsb2dpbiI6InVzZXIwMDEiLCJwYXNzd29yZCI6IjExMTExMSIsImlhdCI6MTYwODkwNzcyNn0.Ow9SlzQPUhiGxCorqiBZahtfihzieN5hvEwrThTrPd4','20201225164846'),(5,2,'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJsb2dpbiI6InVzZXIwMDEiLCJwYXNzd29yZCI6IjExMTExMSIsImlhdCI6MTYwODkxMTIxMn0.mOi78GKoYFjH8O_g5wrFe8U7aQPhdCFcYsGxhhbyUWg','20201225174652'),(6,3,'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJsb2dpbiI6InVzZXIwMDIiLCJwYXNzd29yZCI6IjExMTExMSIsImlhdCI6MTYwODkxMTM2Nn0.-Z0VKrRFLJ_nmgZ9paDdcMIF7LMIQzhxGH5f2vbWQLw','20201225174926'),(7,1,'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJsb2dpbiI6ImFkbWluXyIsInBhc3N3b3JkIjoiMTExMTExIiwiaWF0IjoxNjA4OTExNDk1fQ.1LyxTJM9xQc0nmhJf6xolOViC44Lo1F01E--9n__e6k','20201225175135'),(8,1,'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJsb2dpbiI6ImFkbWluXyIsInBhc3N3b3JkIjoiMTExMTExIiwiaWF0IjoxNjA4OTExNjQwfQ.uUX87uLnZMtnz35dIqPzzLxTVy8R4UJVnV0aRsucnC8','20201225175400'),(9,2,'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJsb2dpbiI6InVzZXIwMDEiLCJwYXNzd29yZCI6IjExMTExMSIsImlhdCI6MTYwODkxMTY2OX0.IheaLcRM3HFqyyHGXYhcdikn2sCnuzs6XSoOu2wFXF8','20201225175429'),(10,1,'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJsb2dpbiI6ImFkbWluXyIsInBhc3N3b3JkIjoiMTExMTExIiwiaWF0IjoxNjA4OTExNzIwfQ.jodDuVWujKHf9LaoiLhN2qiisvlFyDGHQE-ibWoR4Wo','20201225175520'),(11,3,'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJsb2dpbiI6InVzZXIwMDIiLCJwYXNzd29yZCI6IjExMTExMSIsImlhdCI6MTYwODkxMjI4Nn0.a8qunAhGMJqIAiPMDKvXl5A-rMEl8i23Dd9-kCAEnbo','20201225180446'),(12,1,'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJsb2dpbiI6ImFkbWluXyIsInBhc3N3b3JkIjoiMTExMTExIiwiaWF0IjoxNjA4OTEyMzg2fQ.UCM9BL9g7kcphaJIF1D1VEf00wxkGKJEkWyToBLVvXk','20201225180626'),(13,1,'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJsb2dpbiI6ImFkbWluXyIsInBhc3N3b3JkIjoiMTExMTExIiwiaWF0IjoxNjA4OTEyNzU2fQ.XNyxVN8XAz9hn86sjlb5aoTiLIfVMvZ9Rd9n0Yw7H5s','20201225181236');
/*!40000 ALTER TABLE `tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `login` text NOT NULL,
  `password` text NOT NULL,
  `name` text NOT NULL,
  `surename` text NOT NULL,
  `patronymic` text NOT NULL,
  `dir` text NOT NULL,
  `type` int unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `type` (`type`),
  CONSTRAINT `users_ibfk_1` FOREIGN KEY (`type`) REFERENCES `users_types` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'admin_','vLFfghR5tNV3K9DKhmwArV+SbjWAcgZZzIDTnJ0JgCo=','Vladislav','Bogdashev','Igorevich','fG8C6f7MYDbMPTXPxIoxczxCAZx4ZNwVxr9duExlUb4',1),(2,'user001','vLFfghR5tNV3K9DKhmwArV+SbjWAcgZZzIDTnJ0JgCo=','Adrian','Helmsli','Harry','wjFi8GlNa8u4JWIRpGUgW5gy0N+MNeMVhe10EwTWo',2),(3,'user002','vLFfghR5tNV3K9DKhmwArV+SbjWAcgZZzIDTnJ0JgCo=','Samuel','Adamson','Jack','486N4EqSblTFofZLG6vYNa4vQ+73U1Tr1smoayHYV1w',3);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users_access`
--

DROP TABLE IF EXISTS `users_access`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users_access` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `admin` bigint unsigned NOT NULL,
  `user` bigint unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `admin` (`admin`),
  KEY `user` (`user`),
  KEY `id` (`id`),
  CONSTRAINT `users_access_ibfk_1` FOREIGN KEY (`admin`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  CONSTRAINT `users_access_ibfk_2` FOREIGN KEY (`user`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users_access`
--

LOCK TABLES `users_access` WRITE;
/*!40000 ALTER TABLE `users_access` DISABLE KEYS */;
INSERT INTO `users_access` VALUES (1,1,2),(2,1,3);
/*!40000 ALTER TABLE `users_access` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users_groups`
--

DROP TABLE IF EXISTS `users_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users_groups` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `user` bigint unsigned NOT NULL,
  `name` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user` (`user`),
  CONSTRAINT `users_groups_ibfk_1` FOREIGN KEY (`user`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users_groups`
--

LOCK TABLES `users_groups` WRITE;
/*!40000 ALTER TABLE `users_groups` DISABLE KEYS */;
INSERT INTO `users_groups` VALUES (1,1,'Общее подразделение');
/*!40000 ALTER TABLE `users_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users_groups_connections`
--

DROP TABLE IF EXISTS `users_groups_connections`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users_groups_connections` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `usergroup` bigint unsigned NOT NULL,
  `user` bigint unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user` (`user`),
  KEY `users_groups_connections_ibfk_2` (`usergroup`),
  CONSTRAINT `users_groups_connections_ibfk_1` FOREIGN KEY (`user`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  CONSTRAINT `users_groups_connections_ibfk_2` FOREIGN KEY (`usergroup`) REFERENCES `users_groups` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users_groups_connections`
--

LOCK TABLES `users_groups_connections` WRITE;
/*!40000 ALTER TABLE `users_groups_connections` DISABLE KEYS */;
INSERT INTO `users_groups_connections` VALUES (1,1,1),(2,1,2),(3,1,3);
/*!40000 ALTER TABLE `users_groups_connections` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users_types`
--

DROP TABLE IF EXISTS `users_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users_types` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `type` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users_types`
--

LOCK TABLES `users_types` WRITE;
/*!40000 ALTER TABLE `users_types` DISABLE KEYS */;
INSERT INTO `users_types` VALUES (1,'admin'),(2,'moderator'),(3,'user');
/*!40000 ALTER TABLE `users_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users_workpositions`
--

DROP TABLE IF EXISTS `users_workpositions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users_workpositions` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `user` bigint unsigned NOT NULL,
  `wp` bigint unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user` (`user`),
  KEY `wp` (`wp`),
  CONSTRAINT `users_workpositions_ibfk_1` FOREIGN KEY (`user`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  CONSTRAINT `users_workpositions_ibfk_2` FOREIGN KEY (`wp`) REFERENCES `workpositions` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users_workpositions`
--

LOCK TABLES `users_workpositions` WRITE;
/*!40000 ALTER TABLE `users_workpositions` DISABLE KEYS */;
INSERT INTO `users_workpositions` VALUES (5,2,6),(6,3,7),(7,1,6);
/*!40000 ALTER TABLE `users_workpositions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `workpositions`
--

DROP TABLE IF EXISTS `workpositions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `workpositions` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `user` bigint unsigned NOT NULL,
  `name` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `workpositions_ibfk_1` (`user`),
  KEY `id` (`id`),
  KEY `id_2` (`id`),
  CONSTRAINT `workpositions_ibfk_1` FOREIGN KEY (`user`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `workpositions`
--

LOCK TABLES `workpositions` WRITE;
/*!40000 ALTER TABLE `workpositions` DISABLE KEYS */;
INSERT INTO `workpositions` VALUES (6,1,'frontend'),(7,1,'backend'),(8,1,'teamlead');
/*!40000 ALTER TABLE `workpositions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wpconnections`
--

DROP TABLE IF EXISTS `wpconnections`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `wpconnections` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `wp` bigint unsigned NOT NULL,
  `competence` bigint unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `wp` (`wp`),
  KEY `competence` (`competence`),
  CONSTRAINT `wpconnections_ibfk_1` FOREIGN KEY (`wp`) REFERENCES `workpositions` (`id`) ON DELETE CASCADE,
  CONSTRAINT `wpconnections_ibfk_2` FOREIGN KEY (`competence`) REFERENCES `competencies` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wpconnections`
--

LOCK TABLES `wpconnections` WRITE;
/*!40000 ALTER TABLE `wpconnections` DISABLE KEYS */;
INSERT INTO `wpconnections` VALUES (17,6,9),(18,6,12),(19,6,13),(20,7,9),(21,7,14),(22,7,12),(23,7,10);
/*!40000 ALTER TABLE `wpconnections` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-12-25 18:18:09
